/**
*@fileoverview BMI calculator 
*@author k.shaikrishnan@gmail.com( Shai Krishnan Kavuri)
*@apiPath /api/UserData
*
*/

/**@description common Imports */
let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');

let app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

app.post('/api/UserData', async (req, res) => {

    let userData = req.body.userData;
    let observation = req.body.observation;

    //define check parameters
    var outputCode = "";
    var message = "Insufficient Parameters ";
    var predefinedCondition = false;

    //checking whether email is provided or not
    if (userData == undefined || userData == "") {
        outputCode = 100;
        message += " Please enter userData";
        predefinedCondition = true;
    }
    //checking whether password is provided or not
    if (observation == undefined || observation == "") {
        outputCode = 100;
        message += " Please enter observation";
        predefinedCondition = true;
    }
    observation = observation.toLowerCase();
    observation = observation.split(" ").join("");
    let observationArr = ['underweight', 'normalweight', 'overweight', 'moderatelyobese', 'severelyobese', 'veryseverelyobese'];
    const found = observationArr.find(element => element === observation)
    if (found == undefined) {
        outputCode = 100;
        message += " Please enter observation correctly choose one of this: 'under weight', 'normal weight', 'over weight', 'moderately obese', 'severely obese', 'very severely obese' ";
        predefinedCondition = true;
    }

    //if any parameters is missing ,then message will be displayed from here
    if (predefinedCondition) {
        console.log(' <<: predefinedCondition :>> ');
        res.send({ outputCode: parseInt(outputCode), message: message });
    }
    else {
        // observation values object
        let observationValues = {
            'underweight': {
                'min': 0,
                'max': 18.4
            },
            'normalweight': {
                'min': 18.5,
                'max': 24.9
            },
            'overweight': {
                'min': 25,
                'max': 29.9
            },
            'moderatelyobese': {
                'min': 30,
                'max': 34.9
            },
            'severelyobese': {
                'min': 35,
                'max': 39.9
            },
            'veryseverelyobese': {
                'min': 40,
            }
        }
        // code for BMI calculation
        let count = 0;
        await userData.forEach((user) => {
            let weight = Number(user.HeightCm);
            let height= Number(user.WeightKg);
            let BMI = (weight / ((height * height) / 1000)).toFixed(1);
            if(observation === 'veryseverelyobese') {
                if(BMI >= observationValues[observation].min) {
                    count += 1;
                }
            }
            else {
                if(BMI >= observationValues[observation].min && BMI <= observationValues[observation].max ) {
                    count += 1;
                }
            }
        })
        res.send({ 
            outputCode: 200, 
            message: "Successfully Calculated User's BMI Total "+req.body.observation+" users :>> "+count+""
        })
    }
})


let port = 3002;
app.listen(port, () => {
    console.log('app is running at port :>> ', port);
})