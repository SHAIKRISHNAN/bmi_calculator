## 1. Install node.js and npm:

https://nodejs.org/en/


## 2. Install dependencies:

Navigate to the BMI_calculator directory.

Run:

```npm install``` 


## 3. Run the project

Run:

```npm start```


## 4. API Testing 

Go to postman click on `Import` select `Import From Link` and paste the below link in the box
https://www.getpostman.com/collections/8720b2ebdbbcc0899391

